class SessionsController < ApplicationController
  skip_before_action :require_login, only: [:create, :new]

  def new
  end 

  def create
    session_params = params.permit(:email, :password)
    @user = User.find_by(email: session_params[:email])
    if @user && @user.authenticate(session_params[:password])
      if( @user.administrador==true)
        redirect_to new_persona_path
      else
      flash[:notice] = "Login valido!"
      session[:user_id] = @user.id
      redirect_to personas_path
      end
    else
      flash[:notice] = "¡Datos incorrectos verifique por favor!"
      redirect_to new_session_path
    end
  end

  def destroy
    session[:user_id] = nil
    flash[:notice] = "¡Sesión cerrada exitosamente!"
    redirect_to new_session_path
  end
end
