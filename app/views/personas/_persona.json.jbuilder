json.extract! persona, :id, :nombre, :fecha_nacimiento, :lugar_nacimiento, :estado_civil, :genero, :created_at, :updated_at
json.url persona_url(persona, format: :json)
