class User < ApplicationRecord
    has_secure_password

    def welcome
        "¡Hola, #{self.email}!"
      end
end
