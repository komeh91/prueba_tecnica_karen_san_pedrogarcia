Rails.application.routes.draw do 
  get 'sessions/new'
  get '/ingresar',to: 'sessions#new'
  get 'sessions/create'
  get 'sessions/destroy' 
  get 'users/create' 
  get 'users/new' 
   
  get 'welcome/index'
  resources :personas
  resources :users, only: [:new, :create, :index, :show]
  resources :sessions, only: [:new, :create, :destroy]
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index" 
  root 'welcome#index'
end
