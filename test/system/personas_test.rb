require "application_system_test_case"

class PersonasTest < ApplicationSystemTestCase
  setup do
    @persona = personas(:one)
  end

  test "visiting the index" do
    visit personas_url
    assert_selector "h1", text: "Personas"
  end

  test "should create persona" do
    visit personas_url
    click_on "New persona"

    fill_in "Estado civil", with: @persona.estado_civil
    fill_in "Fecha nacimiento", with: @persona.fecha_nacimiento
    fill_in "Genero", with: @persona.genero
    fill_in "Lugar nacimiento", with: @persona.lugar_nacimiento
    fill_in "Nombre", with: @persona.nombre
    click_on "Registrar persona"

    assert_text "Registro de persona creado exitosamente"
    click_on "Regresar"
  end

  test "should update Persona" do
    visit persona_url(@persona)
    click_on "Editar registro de persona", match: :first

    fill_in "Estado civil", with: @persona.estado_civil
    fill_in "Fecha nacimiento", with: @persona.fecha_nacimiento
    fill_in "Genero", with: @persona.genero
    fill_in "Lugar nacimiento", with: @persona.lugar_nacimiento
    fill_in "Nombre", with: @persona.nombre
    click_on "Update Persona"

    assert_text "Registro de persona editado correctamente"
    click_on "Regresar"
  end

  test "should destroy Persona" do
    visit persona_url(@persona)
    click_on "Eliminar persona", match: :first

    assert_text "Registro de persona eliminado correctamente"
  end
end
